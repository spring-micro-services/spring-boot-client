package com.springboot;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.InetAddress;
import java.net.UnknownHostException;

@RestController
public class ClientController {

    @Autowired
    private HelloClient helloClient;

    @RequestMapping("/call")
    public String hello() throws UnknownHostException {
        String hello = helloClient.callHelloService();
        return String.format("Client @ %s, received: %s", InetAddress.getLocalHost().getHostName(), hello);
    }

}
