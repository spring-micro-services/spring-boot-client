package com.springboot;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * Created by ukomu01 on 27/01/16.
 */
@Service
public class HelloClient {

    Logger log = Logger.getLogger(HelloClient.class.getName());

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "defaultHelloMessage")
    public String callHelloService() {
        log.info("Calling hello service");
        return restTemplate.getForObject("http://spring-boot-service/hello", String.class);
    }

    private String defaultHelloMessage(){
        log.info("Circuit Breaker is open. Returning default message.");
        return "Hello from Hystrix. Circuit Breaker is open!";
    }
}
